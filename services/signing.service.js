'use strict'

const PassService = require('./pass.service');
const TokenService = require('./token.service');
const bcrypt = require('bcrypt');
const mongojs = require('mongojs');
const moment = require('moment');

const DBNAME = require('../config').dbName;

// Función para convertir un id textual en un objectID
var id = mongojs.ObjectId;

// Conectamos con la base de datos
var db = mongojs(DBNAME);

// signIn
//
// Realiza un Login en el sistema. Devuelve un "result", el token y el usuario correspondiente
function signIn( e_name, e_pass ) {
    return new Promise( ( resolve, reject ) => {
        try {
            let elementoNuevo = { lastLogin: moment().unix() };
            db.collection('user').findOne({email: e_name}, (err, elemento) => {
                if (err) return next(err);
                if (!elemento) {
                    reject(
                        {
                        result: '500',
                        message: 'Usuario y/o contraseña incorrectos'
                        });
                }
                else if(elemento.password) {
                    PassService.encriptaPassword(e_pass)
                    .then( hash => {
                        PassService.comparaPassword(e_pass, elemento.password)
                        .then( comparacion => {
                            if (comparacion) {
                                db.collection('user').update({_id: id(elemento._id)},
                        {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) => {
                                    if (err) { return next(err) }
                                    db.collection('user').findOne({email: e_name}, (err, modificado) => {
                                        let tkn = TokenService.creaToken(modificado);
                                        if (err) return next(err);
                                        resolve(
                                            {
                                            result: 'OK',
                                            token: tkn,
                                            usuario: modificado
                                            });
                                        });
                                    });
                                }
                                else reject(
                                    {
                                    result: '500',
                                    message: 'Usuario y/o contraseña incorrectos'
                                    });
                            });
                    });
                }
                else reject(
                    {
                    result: '500',
                    message: 'Usuario no tiene campo <pass> o <password>'
                    });
            });
        } catch {
            reject( {
                result: 500,
                message: 'Error en el SignIn'
            });
        }
    });
}


// signUp
// Este controlador realiza un registro mínimo del usuario. Solo datos para Login.
function signUp( e_name, e_email, e_pass ) {
    return new Promise( ( resolve, reject ) => {
        try {
            db.collection('user').findOne({email: e_name}, (err, elemento) => {
                if (err) return next(err);
                if (elemento) {
                    reject(
                        {
                        result: '500',
                        message: 'Ya existe un usuario con este email'
                        });
                }
                else {
                    PassService.encriptaPassword(e_pass)
                    .then( hash => {
                        let usuario = {
                            email: e_email,
                            displayName: e_name,
                            password: hash,
                            signupDate: moment().unix(),
                            lastLogin: moment().unix()
                        };
                        db.collection('user').save(usuario, (err, coleccionGuardada) => {
                            if (err) { return next(err) }
                            let tkn = TokenService.creaToken(coleccionGuardada);
                            resolve(
                                {
                                result: 'OK',
                                token: tkn,
                                usuario: coleccionGuardada
                                });
                        });
                    });
                }
            });
        } catch {
            reject( {
                result: 500,
                message: 'Error en el SignUp'
            });
        }
    });
}

module.exports = {
    signIn,
    signUp
};