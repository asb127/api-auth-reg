'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');

const SECRET = require('../config').secret;
const EXP_TIME = require('../config').tokenExpTmp;

// creaToken
//
// Devuelve un token JWT
// Formato:
//    HEADER.PAYLOAD.VERIFY_SIGNATURE
// Donde:
//    HEADER (Objeto JSON con el al...)
//        {
//            alg: ...
// ...
//    VERIFY_SIGNATURE = HMACSHA256 ( base64UrlEncode(HEAD) + "." + base64UrlEncode(PAYLOAD), SECRET)
//
function creaToken( user ) {
    const payload = {
        sub: user._id,
        iat: moment().unix(),
        exp: moment().add(EXP_TIME, 'minutes').unix()
    };
    return jwt.encode( payload, SECRET );
}

// decodificaToken
//
// Devuelve el identificador del usuario
//
function decodificaToken( token ) {
    return new Promise( ( resolve, reject ) => {
        try {
            const payload = jwt.decode( token, SECRET, true );
            if( payload.exp <= moment().unix() ) {
                reject({
                    result: 401,
                    message: 'El token ha caducado'
                });
            }
            resolve( payload.sub );
        } catch {
            reject( {
                result: 500,
                message: 'El token no es válido'
            });
        }
    });
}

module.exports = {
    creaToken,
    decodificaToken
};