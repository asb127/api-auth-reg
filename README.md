# Servicio Web RESTful de Registro y Autenticación

_Ejemplo de WS RESTful de registro y autenticación._

## Comenzando 🚀

Estas instrucciones permitirán obtener una copia del proyecto en funcionamiento en cualquier máquina para propósitos de desarrollo y pruebas. La máquina virtual empleada para realizar esta práctica utiliza [Ubuntu 20.04 LTS](https://ubuntu.com/download/desktop), por lo que las instrucciones proporcionadas serán las adecuadas para un sistema que emplee **Ubuntu 20.04**.

Para obtener todos los archivos de la práctica, ejecuta la siguiente línea de código:
```
$ git clone https://asb127@bitbucket.org/asb127/api-auth-reg.git
```

### Pre-requisitos 📋

Para la realización de esta práctica es necesario instalar [NodeJS](https://nodejs.org/en/). Primero, se instala el gestor de paquetes de Node (npm):

```
$ sudo apt update
$ sudo apt install npm
```

A continuación, se instala una utilidad para instalar y mantener las versiones de Node (llamada **n**):

```
$ sudo npm clean -f
$ sudo npm i -g n
```

Y se instala la última versión estable de Node:

```
$ sudo n stable
```

También es necesario instalar las bibliotecas **express** (para facilitar la gestión de métodos y recursos HTTP con **NodeJS**, **bcrypt** (para poder encriptar contraseñas mediante hash), **moment** (para poder analizar, obtener, manipular y dar formato a fechas) y **jwt-simple** (para poder crear JWTs, JSON Web Tokens):

```
$ npm i -S express
$ npm i -S bcrypt
$ npm i -S moment
$ npm i -S jwt-simple;
```

Finalmente, se deben instalar un **JSON Formatter** (para tener mejor visualización de las respuestas del API), [Postman](https://www.postman.com/) (para hacer invocaciones al servidor), y **Morgan** (motor de registro del servidor).

```
$ npm i -S morgan
```


### Instalación 🔧

El primer paso es instalar MongoDB, una sistema de gestión de bases de datos no estructuradas. Se instala de siguiente forma:

```
$ sudo apt update
$ sudo apt install -y mongodb
```

El comando **systemctl** se usa para lanzar y gestionar el servicio:

```
$ sudo systemctl start mongodb
```

Y verificar su funcionamiento así:

```
$ mongo --eval 'db.runCommand({ connectionStatus: 1 })'
MongoDB shell version v3.6.8
connecting to: mongodb://127.0.0.1:27017
Implicit session: session { "id" : UUID("b9086bea-a3f0-4f51-b759-4b95af37b0a8") }
MongoDB server version: 3.6.8
{
	"authInfo" : {
		"authenticatedUsers" : [ ],
		"authenticatedUserRoles" : [ ]
	},
	"ok" : 1
}

```

En otra terminal se abre el cliente *mongo* (gestor de la base de datos), pudiendo utilizarla propia terminal para mandar comandos y gestionarla:

```
$ mongo --host 127.0.0.1:27017
> show dbs
```

Y se instalan las bibliotecas **mongodb** y **mongojs** en nuestro proyecto, que nos permitirán trabajar con la base de datos y acceder de manera sencilla a ella desde **node**.

```
$ cd
$ cd node/api-rest
$ npm i -S mongodb
$ npm i -S mongojs
```

Creamos un archivo **config.js** y lo abrimos en un editor de código para modificarlo. En este archivo definimos valores que se usarán en nuestro servicio, como el nombre de la BD a usar, el secreto para los JWT o el tiempo de validez de estos token:

```
$ cd ~/node/api-rest
$ touch config.js
$ code .
```
```js
module.exports = {
    dbName: 'SD',
    port: 3000,
    secret: 'mipalabrasecreta',
    tokenExpTmp: 7*24*60 // 1 semana de validez para cada token
}
```

A continuación, utilizaremos los servicios que aparecen en la Guía 5 (auth-test). Estos servicios se encuentran en la carpeta **services**. Estos archivos se pueden obtener ejecutando el siguiente comando:
```
$ git clone https://asb127@bitbucket.org/asb127/auth-test.git
```

Los servicios a utilizar son los siguientes:

- **pass.service.js** (encripta y compara contraseñas usando funciones hash):
```js
'use strict'

const bcrypt = require('bcrypt');

// encriptaPassword
//
// Devuelve un hash con salt en el formato
//  $2b$10$.GFMCa5rH9wQiwRzDwb4S.QQDfXSU3l8HpCpDf1QHiQo5CkEgANxa
//  ****.. **********************+++++++++++++++++++++++++++++++
//  Alg Cost      Salt                     Hash
function encriptaPassword( password ) {
    return bcrypt.hash ( password, 10 );
}

// comparaPassword
// Devolver true o false si coinciden o no el hash y el pass
function comparaPassword( password, hash ) {
    return bcrypt.compare ( password, hash );
}

module.exports = {
    encriptaPassword,
    comparaPassword
};
```

- **token.service.js** (crea un token JWT a partir de una ID de usuario y decodifica un token JWT para obtener la ID de usuario siempre y cuando el token sea válido):
```js
'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');

const SECRET = require('../config').secret;
const EXP_TIME = require('../config').tokenExpTmp;

// creaToken
//
// Devuelve un token JWT
// Formato:
//    HEADER.PAYLOAD.VERIFY_SIGNATURE
// Donde:
//    HEADER (Objeto JSON con el al...)
//        {
//            alg: ...
// ...
//    VERIFY_SIGNATURE = HMACSHA256 ( base64UrlEncode(HEAD) + "." + base64UrlEncode(PAYLOAD), SECRET)
//
function creaToken( user ) {
    const payload = {
        sub: user._id,
        iat: moment().unix(),
        exp: moment().add(EXP_TIME, 'minutes').unix()
    };
    return jwt.encode( payload, SECRET );
}

// decodificaToken
//
// Devuelve el identificador del usuario
//
function decodificaToken( token ) {
    return new Promise( ( resolve, reject ) => {
        try {
            const payload = jwt.decode( token, SECRET, true );
            if( payload.exp <= moment().unix() ) {
                reject({
                    result: 401,
                    message: 'El token ha caducado'
                });
            }
            resolve( payload.sub );
        } catch {
            reject( {
                result: 500,
                message: 'El token no es válido'
            });
        }
    });
}

module.exports = {
    creaToken,
    decodificaToken
};
```

Además, crearemos un servicio adicional en esta carpeta, que llamaremos **signing.service.js**, el cual contendrá las funciones *signIn* y *signUp* requeridas para nuestro programa:

```js
'use strict'

const PassService = require('./pass.service');
const TokenService = require('./token.service');
const bcrypt = require('bcrypt');
const mongojs = require('mongojs');
const moment = require('moment');

const DBNAME = require('../config').dbName;

// Función para convertir un id textual en un objectID
var id = mongojs.ObjectId;

// Conectamos con la base de datos
var db = mongojs(DBNAME);

// signIn
//
// Realiza un Login en el sistema. Devuelve un "result", el token y el usuario correspondiente
function signIn( e_name, e_pass ) {
    return new Promise( ( resolve, reject ) => {
        try {
            let elementoNuevo = { lastLogin: moment().unix() };
            db.collection('user').findOne({email: e_name}, (err, elemento) => {
                if (err) return next(err);
                if (!elemento) {
                    reject(
                        {
                        result: '500',
                        message: 'Usuario y/o contraseña incorrectos'
                        });
                }
                else if(elemento.password) {
                    PassService.encriptaPassword(e_pass)
                    .then( hash => {
                        PassService.comparaPassword(e_pass, elemento.password)
                        .then( comparacion => {
                            if (comparacion) {
                                db.collection('user').update({_id: id(elemento._id)},
                        {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) => {
                                    if (err) { return next(err) }
                                    db.collection('user').findOne({email: e_name}, (err, modificado) => {
                                        let tkn = TokenService.creaToken(modificado);
                                        if (err) return next(err);
                                        resolve(
                                            {
                                            result: 'OK',
                                            token: tkn,
                                            usuario: modificado
                                            });
                                        });
                                    });
                                }
                                else reject(
                                    {
                                    result: '500',
                                    message: 'Usuario y/o contraseña incorrectos'
                                    });
                            });
                    });
                }
                else reject(
                    {
                    result: '500',
                    message: 'Usuario no tiene campo <pass> o <password>'
                    });
            });
        } catch {
            reject( {
                result: 500,
                message: 'Error en el SignIn'
            });
        }
    });
}


// signUp
// Este controlador realiza un registro mínimo del usuario. Solo datos para Login.
function signUp( e_name, e_email, e_pass ) {
    return new Promise( ( resolve, reject ) => {
        try {
            db.collection('user').findOne({email: e_name}, (err, elemento) => {
                if (err) return next(err);
                if (elemento) {
                    reject(
                        {
                        result: '500',
                        message: 'Ya existe un usuario con este email'
                        });
                }
                else {
                    PassService.encriptaPassword(e_pass)
                    .then( hash => {
                        let usuario = {
                            email: e_email,
                            displayName: e_name,
                            password: hash,
                            signupDate: moment().unix(),
                            lastLogin: moment().unix()
                        };
                        db.collection('user').save(usuario, (err, coleccionGuardada) => {
                            if (err) { return next(err) }
                            let tkn = TokenService.creaToken(coleccionGuardada);
                            resolve(
                                {
                                result: 'OK',
                                token: tkn,
                                usuario: coleccionGuardada
                                });
                        });
                    });
                }
            });
        } catch {
            reject( {
                result: 500,
                message: 'Error en el SignUp'
            });
        }
    });
}

module.exports = {
    signIn,
    signUp
};
```

Finalmente, abrimos el archivo **index.js** en un editor de código para crear nuestro servicio API RESTful:

```js
'use strict'

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const SigningService = require('./services/signing.service');
const PassService = require('./services/pass.service');
const TokenService = require('./services/token.service');
const config_port = require('./config').port;
const port = process.env.PORT || config_port;
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const moment = require('moment');
const cors = require('cors');

const app = express();
const DBNAME = require('./config').dbName;

// Conectamos con la base de datos
var db = mongojs(DBNAME);

// Función para convertir un id textual en un objectID
var id = mongojs.ObjectId;

// Declaramos los middleware
var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "*");
    return next();
}

var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
}

// Función para verificar los tokens
var auth = (req, res, next) => {
    if (req.headers.authorization != null) {
        const jwt = req.headers.authorization.split(" ")[1];
        TokenService.decodificaToken( jwt )
        .then( userId => {
            req.user = {
                id: userId
            };
            return next();
        })
        .catch( err => { 
            let estatus = err.result;
            res.status(estatus).json(err);
        });    
    }
    else {
        return next(new Error("No hay token de autorización"))
    }
}

app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }))
app.use(express.json());
app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);


// Rutas API

// GET /api/user
// Obtenemos todos los usuarios registrados en el sistema.
app.get('/api/user', auth, (req, res, next) => {
    db.collection('user').find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

// GET /api/user/{id}
// Obtenemos el usuario indicado en {id}.
app.get('/api/user/:id', auth, (req, res, next) => {
    db.collection('user').findOne({_id: id(req.params.id) }, (err, elemento) => {
        if (err) return next(err);
        if(!elemento){
            res.status(404).json ({
                result: 'Error: No encontrado',
                message: 'No se encontró ningún usuario con la ID indicada'
            });
        }
        else res.json(elemento);
    });
});

// POST /api/user
// Registramos un nuevo usuario con toda su información.
app.post('/api/user', auth, (req, res, next) => {
    const elemento = req.body;

    if (!elemento.name || !elemento.email || !elemento.pass) {
        res.status(400).json ({
            result: 'Error: Bad data',
            message: 'Se precisan los campos <name>, <email> y <pass>'
        });
    } else {
        db.collection('user').findOne({email: e_name}, (err, elemento) => {
            if (err) return next(err);
            if (elemento) {
                reject(
                    {
                    result: '500',
                    message: 'Ya existe un usuario con este email'
                    });
            }
            else {
                let usuario = elemento;
                usuario.displayName = usuario.name;
                PassService.encriptaPassword(usuario.pass)
                .then( clave => {
                    usuario.password = clave;
                    usuario.singUpDate = moment().unix();
                    usuario.lastLogin = null;
                    delete usuario.name;
                    delete usuario.pass;
                    db.collection('user').save(usuario, (err, coleccionGuardada) => {
                        if (err) return next(err);
                        res.json(coleccionGuardada);
                    });
                });
            }
        });
    }
});

// PUT /api/user/{id}
// Modificamos el usuario {id}.
app.put('/api/user/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;
    db.collection('user').update({_id: id(elementoId) },
            {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) => {
        if (err) return next(err);
        res.json(elementoModif);
    });
});

// DELETE /api/user/{id}
// Eliminamos el usuario {id}.
app.delete('/api/user/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;

    db.collection('user').remove({_id: id(elementoId) }, (err, resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});

// POST /api/auth/reg
// Realiza un registro mínimo (signUp) de un usuario y devuelve un token válido.
app.post('/api/auth/reg', (req, res, next) => {
    const elemento = req.body;

    if (!elemento.name || !elemento.email || !elemento.pass) {
        res.status(400).json ({
            result: 'Error: Bad data',
            message: 'Se precisan los campos <name>, <email> y <pass>'
        });
    } else {
        SigningService.signUp(elemento.name, elemento.email, elemento.pass)
        .then( respuesta => {
            return res.json(respuesta);
        })
        .catch ( err => {
            return next(err);
        });
    }
});

// POST /api/auth
// Realiza una identificación o login (signIn) y devuelve un token válido.
app.post('/api/auth', (req, res, next) => {
    const elemento = req.body;

    if (!elemento.email || !elemento.pass) {
        res.status(400).json ({
            result: 'Error: Bad data',
            message: 'Se precisan los campos <email> y <pass>'
        });
    } else {
        SigningService.signIn(elemento.email, elemento.pass)
        .then( respuesta => {
            return res.json(respuesta);
        })
        .catch ( err => { return res.json(err); });
    }
});

// GET /api/auth
// Obtenemos todos los usuarios registrados en el sistema.
// Mostramos versión reducida de GET /api/user.
app.get('/api/auth', auth, (req, res, next) => {
    db.collection('user').find({} , {email: 1, name: 1, displayName: 1, _id: 0}, (err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

// GET /api/auth/me
// Obtenemos el usuario a partir de un token válido.
app.get('/api/auth/me', auth, (req, res, next) => {
    db.collection('user').findOne({_id: id(req.user.id) }, (err, elemento) => {
        if (err) return next(err);
        res.json(elemento);
    });
});

// Lanzamos nuestro servicio API
https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`SEC WS API REST CRUD con DB ejecutándose en https://localhost:${port}/api`);
});
```

Y se inicia el servidor:
```
$ npm start
```

Finalmente, utilizando Postman, creamos un API para realizar llamadas al servidor:

![Llamadas API](./img/Llamadas%20API.jpg)  

Mediante estas llamadas se podrá realizar solicitudes al servidor.

## Ejecutando las pruebas ⚙️

### Pruebas Postman
Probamos con Postman que el API funcione correctamente:  

**Prueba GET**: Obtener usuarios en la base de datos

```
GET https://localhost:3000/api/user/
Auth (bearer): "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MjQ5Yzc3NDhkMmU0NjMzYjQ3NjlkNTAiLCJpYXQiOjE2NDkwMjI4MDMsImV4cCI6MTY0OTYyNzYwM30.y4AH-nqiRMpszE503ldCGY1m2AxFzYZI2NPkjzM2XNU"
```

Obtenemos la siguiente respuesta:

![Respuesta GET api/user](./img/GET%20api-user.jpg)  


**Prueba GET**: Obtener usuario por ID

```
GET https://localhost:3000/api/user/6248880b0049891bc741003f
Auth (bearer): "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MjQ5Yzc3NDhkMmU0NjMzYjQ3NjlkNTAiLCJpYXQiOjE2NDkwMjI4MDMsImV4cCI6MTY0OTYyNzYwM30.y4AH-nqiRMpszE503ldCGY1m2AxFzYZI2NPkjzM2XNU"
```

Obtenemos la siguiente respuesta:

![Respuesta GET api/user/{id}](./img/GET%20api-user-id.jpg)

**Prueba POST**: Incorporación de un usuario

```
POST https://localhost:3000/api/user/
Auth (bearer): "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MjQ5Yzc3NDhkMmU0NjMzYjQ3NjlkNTAiLCJpYXQiOjE2NDkwMjI4MDMsImV4cCI6MTY0OTYyNzYwM30.y4AH-nqiRMpszE503ldCGY1m2AxFzYZI2NPkjzM2XNU"
Cabecera (raw, text): Content-Type:application/json
Cuerpo (raw, JSON)
    {
    "name": "Gustavo Sevilla",
    "email": "gsevilla@dtic.ua.es",
    "pass": "C0ntra",
    "edad": 29,
    "empleo": "Conserje"
    }
```

Obtenemos la siguiente respuesta:

![Respuesta POST api/user](./img/POST%20api-user.jpg)


**Prueba PUT**: Modificación de los datos de un usuario

```
PUT https://localhost:3000/api/user/62488f73b2e47f2062474150
Auth (bearer): "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MjQ5Yzc3NDhkMmU0NjMzYjQ3NjlkNTAiLCJpYXQiOjE2NDkwMjI4MDMsImV4cCI6MTY0OTYyNzYwM30.y4AH-nqiRMpszE503ldCGY1m2AxFzYZI2NPkjzM2XNU"
Cabecera (raw, text): Content-Type:application/json
Cuerpo (raw, JSON)
    {
    "displayName": "Cristóbal González",
    "email": "cristobal_gonzalez@ua.es",
    "edad": 44
    }
```

Obtenemos la siguiente respuesta:

![Respuesta PUT api/user/{id}](./img/PUT%20api-user-id.jpg)

Comprobamos los cambios con un GET:
```
GET https://localhost:3000/api/user/62488f73b2e47f2062474150
Auth (bearer): "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MjQ5Yzc3NDhkMmU0NjMzYjQ3NjlkNTAiLCJpYXQiOjE2NDkwMjI4MDMsImV4cCI6MTY0OTYyNzYwM30.y4AH-nqiRMpszE503ldCGY1m2AxFzYZI2NPkjzM2XNU"
```

Obtenemos la siguiente respuesta:

![Respuesta GET api/user/{id}](./img/PUT%20GET%20api-user-id.jpg)


**Prueba DELETE**: Borrado de un usuario de la base de datos

```
DELETE http://localhost:3000/api/familia/621f5279a4af400c90846b7d
Auth (bearer): "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MjQ5Yzc3NDhkMmU0NjMzYjQ3NjlkNTAiLCJpYXQiOjE2NDkwMjI4MDMsImV4cCI6MTY0OTYyNzYwM30.y4AH-nqiRMpszE503ldCGY1m2AxFzYZI2NPkjzM2XNU"
```

Obtenemos la siguiente respuesta:

![Respuesta DELETE api/user/{id}](./img/DELETE%20api-user-id.jpg)  

Comprobamos los cambios con un GET:
```
GET http://localhost:3000/api/familia/621f5279a4af400c90846b7d
Auth (bearer): "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MjQ5Yzc3NDhkMmU0NjMzYjQ3NjlkNTAiLCJpYXQiOjE2NDkwMjI4MDMsImV4cCI6MTY0OTYyNzYwM30.y4AH-nqiRMpszE503ldCGY1m2AxFzYZI2NPkjzM2XNU"
```

![Respuesta GET api/user/{id}](./img/DELETE%20GET%20api-user-id.jpg)


**Prueba POST**: Registro de un usuario (signUp)

```
POST https://localhost:3000/api/auth/reg/
Cabecera (raw, text): Content-Type:application/json
Cuerpo (raw, JSON)
    {
    "name": "Felipe Gómez",
    "email": "fgomez@dtic.ua.es",
    "pass": "Felipe"
    }
```

Obtenemos la siguiente respuesta:

![Respuesta POST api/auth/reg](./img/POST%20api-auth-reg.jpg)


**Prueba POST**: Login de un usuario (signIn)

```
POST https://localhost:3000/api/auth/
Cabecera (raw, text): Content-Type:application/json
Cuerpo (raw, JSON)
    {
    "email": "fgomez@dtic.ua.es",
    "pass": "Felipe"
    }
```

Obtenemos la siguiente respuesta:

![Respuesta POST api/auth](./img/POST%20api-auth.jpg)


**Prueba GET**: Obtener todos los usuarios registrados, versión reducida de *GET api/user* 

```
GET https://localhost:3000/api/auth/
Auth (bearer): "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MjQ5Yzc3NDhkMmU0NjMzYjQ3NjlkNTAiLCJpYXQiOjE2NDkwMjI4MDMsImV4cCI6MTY0OTYyNzYwM30.y4AH-nqiRMpszE503ldCGY1m2AxFzYZI2NPkjzM2XNU"
```

Obtenemos la siguiente respuesta:

![Respuesta GET api/auth](./img/GET%20api-auth.jpg)


**Prueba GET**: Obtener el usuario a partir de un token válido 

```
GET https://localhost:3000/api/auth/me
Auth (bearer): "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MjQ5Yzc3NDhkMmU0NjMzYjQ3NjlkNTAiLCJpYXQiOjE2NDkwMjI4MDMsImV4cCI6MTY0OTYyNzYwM30.y4AH-nqiRMpszE503ldCGY1m2AxFzYZI2NPkjzM2XNU"
```

Obtenemos la siguiente respuesta:

![Respuesta GET api/auth/me](./img/GET%20api-auth-me.jpg)

## Construido con 🛠️

* [VS Code](https://code.visualstudio.com/) - Editor de código usado para programar
* [MongoDB](https://www.mongodb.com/) - Sistema de base de datos NoSQL
* [Postman](https://www.postman.com/) - Aplicación para realización de las llamadas API  

## Versionado 📌

Para todas las versiones disponibles, mira los [commits en este repositorio](https://bitbucket.org/asb127/api-auth-reg/commits/).

## Autores ✒️

* **Paco Maciá** - *Guía de la práctica* - [pmacia](https://github.com/pmacia)
* **Antonio Sánchez** - *Trabajo y documentación* - [asb127](https://bitbucket.org/asb127/)