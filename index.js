'use strict'

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const SigningService = require('./services/signing.service');
const PassService = require('./services/pass.service');
const TokenService = require('./services/token.service');
const config_port = require('./config').port;
const port = process.env.PORT || config_port;
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const moment = require('moment');
const cors = require('cors');

const app = express();
const DBNAME = require('./config').dbName;

// Conectamos con la base de datos
var db = mongojs(DBNAME);

// Función para convertir un id textual en un objectID
var id = mongojs.ObjectId;

// Declaramos los middleware
var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "*");
    return next();
}

var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
}

// Función para verificar los tokens
var auth = (req, res, next) => {
    if (req.headers.authorization != null) {
        const jwt = req.headers.authorization.split(" ")[1];
        TokenService.decodificaToken( jwt )
        .then( userId => {
            req.user = {
                id: userId
            };
            return next();
        })
        .catch( err => { 
            let estatus = err.result;
            res.status(estatus).json(err);
        });    
    }
    else {
        return next(new Error("No hay token de autorización"))
    }
}

app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }))
app.use(express.json());
app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);


// Rutas API

// GET /api/user
// Obtenemos todos los usuarios registrados en el sistema.
app.get('/api/user', auth, (req, res, next) => {
    db.collection('user').find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

// GET /api/user/{id}
// Obtenemos el usuario indicado en {id}.
app.get('/api/user/:id', auth, (req, res, next) => {
    db.collection('user').findOne({_id: id(req.params.id) }, (err, elemento) => {
        if (err) return next(err);
        if(!elemento){
            res.status(404).json ({
                result: 'Error: No encontrado',
                message: 'No se encontró ningún usuario con la ID indicada'
            });
        }
        else res.json(elemento);
    });
});

// POST /api/user
// Registramos un nuevo usuario con toda su información.
app.post('/api/user', auth, (req, res, next) => {
    const elemento = req.body;

    if (!elemento.name || !elemento.email || !elemento.pass) {
        res.status(400).json ({
            result: 'Error: Bad data',
            message: 'Se precisan los campos <name>, <email> y <pass>'
        });
    } else {
        db.collection('user').findOne({email: e_name}, (err, elemento) => {
            if (err) return next(err);
            if (elemento) {
                reject(
                    {
                    result: '500',
                    message: 'Ya existe un usuario con este email'
                    });
            }
            else {
                let usuario = elemento;
                usuario.displayName = usuario.name;
                PassService.encriptaPassword(usuario.pass)
                .then( clave => {
                    usuario.password = clave;
                    usuario.singUpDate = moment().unix();
                    usuario.lastLogin = null;
                    delete usuario.name;
                    delete usuario.pass;
                    db.collection('user').save(usuario, (err, coleccionGuardada) => {
                        if (err) return next(err);
                        res.json(coleccionGuardada);
                    });
                });
            }
        });
    }
});

// PUT /api/user/{id}
// Modificamos el usuario {id}.
app.put('/api/user/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;
    db.collection('user').update({_id: id(elementoId) },
            {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) => {
        if (err) return next(err);
        res.json(elementoModif);
    });
});

// DELETE /api/user/{id}
// Eliminamos el usuario {id}.
app.delete('/api/user/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;

    db.collection('user').remove({_id: id(elementoId) }, (err, resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});

// POST /api/auth/reg
// Realiza un registro mínimo (signUp) de un usuario y devuelve un token válido.
app.post('/api/auth/reg', (req, res, next) => {
    const elemento = req.body;

    if (!elemento.name || !elemento.email || !elemento.pass) {
        res.status(400).json ({
            result: 'Error: Bad data',
            message: 'Se precisan los campos <name>, <email> y <pass>'
        });
    } else {
        SigningService.signUp(elemento.name, elemento.email, elemento.pass)
        .then( respuesta => {
            return res.json(respuesta);
        })
        .catch ( err => {
            return next(err);
        });
    }
});

// POST /api/auth
// Realiza una identificación o login (signIn) y devuelve un token válido.
app.post('/api/auth', (req, res, next) => {
    const elemento = req.body;

    if (!elemento.email || !elemento.pass) {
        res.status(400).json ({
            result: 'Error: Bad data',
            message: 'Se precisan los campos <email> y <pass>'
        });
    } else {
        SigningService.signIn(elemento.email, elemento.pass)
        .then( respuesta => {
            return res.json(respuesta);
        })
        .catch ( err => { return res.json(err); });
    }
});

// GET /api/auth
// Obtenemos todos los usuarios registrados en el sistema.
// Mostramos versión reducida de GET /api/user.
app.get('/api/auth', auth, (req, res, next) => {
    db.collection('user').find({} , {email: 1, name: 1, displayName: 1, _id: 0}, (err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

// GET /api/auth/me
// Obtenemos el usuario a partir de un token válido.
app.get('/api/auth/me', auth, (req, res, next) => {
    db.collection('user').findOne({_id: id(req.user.id) }, (err, elemento) => {
        if (err) return next(err);
        res.json(elemento);
    });
});

// Lanzamos nuestro servicio API
https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`SEC WS API REST CRUD con DB ejecutándose en https://localhost:${port}/api`);
});